// Set the engine on
#define OLC_PGE_APPLICATION

/* 
 * Observation: There is a clear limitation on this algorthim.
 * We can't increment the counting variables forever and converge as much
 * as we would like to. Although we are using 'unsigned long long', this
 * obviously still means we have a limit of bits to represent integer numbers.
 */


// Main Includes
#include "olcPixelGameEngine.h"

// Standard Includes
#include <string>
#include <iostream>
#include <time.h>

// Namespaces so we can have a cleaner code
using namespace olc;
using namespace std;

// Demo Properties
static const int RADIUS = 400;
static const int SCREEN_DIM = 820;
static const int BORDER = 10;

// Implement the PixelGameEngine abstract class
class MonteCarlo : public PixelGameEngine
{   private:
        unsigned long long dotsIn;
        unsigned long long dotsTotal;

    bool OnUserCreate() override
    {   
        dotsIn  = 0;
        dotsTotal = 0;

        srand(time(NULL));

        DrawCircle(SCREEN_DIM / 2, SCREEN_DIM / 2, RADIUS, WHITE);
        DrawRect(BORDER, BORDER, 2 * RADIUS, 2 * RADIUS, WHITE);

        return true;
    }

    bool OnUserUpdate(float fElapsedTime) override
    {   
        for (int i = 0; i < 10; i++) { // Converge 10 times faster per frame, theorically

            int x = (rand() % (2 * RADIUS)) + BORDER;
            int y = (rand() % (2 * RADIUS)) + BORDER;
            int c = SCREEN_DIM / 2;

            double distFromCenterSquared = (x - c) * (x - c) + (y - c) * (y - c);

            if (distFromCenterSquared < RADIUS * RADIUS) {
                FillCircle(x, y, 1, GREEN);
                dotsIn++;
            }
            dotsTotal++;

            double estimatedPI = (4.0 * dotsIn) / dotsTotal;

            FillRect(20, 20, 150, 50, BLACK);
            DrawString(20, 20, to_string(estimatedPI), WHITE, 2);
        }

        return true;
    }
};

int main()
{
    MonteCarlo demo;
    if (demo.Construct(SCREEN_DIM, SCREEN_DIM, 4, 4)) {
        demo.Start();
    }
    return EXIT_SUCCESS;
}