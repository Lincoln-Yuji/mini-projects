# Mini Projects

Little computer science problems. Nothing too serious.
Most of them use the One Lone Coder's C++ game engine.

# OLC Pixel Game Engine source repository and documentation:

https://github.com/OneLoneCoder/olcPixelGameEngine

That includes its wiki and the required dependencies.