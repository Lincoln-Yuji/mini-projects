build:
	@if [ ! -d "bin" ]; then \
		mkdir bin; \
	fi
	@read srcdir; \
	if [ -f $$srcdir/main.cpp ]; then \
		g++ -o bin/main $$srcdir/main.cpp -I /home/lincoln/.local/lib -lX11 -lGL -lpthread -lpng -lstdc++fs -std=c++17; \
	else \
		echo "[ ERRO ] $$srcdir/main.cpp não existe!"; \
	fi

clear:
	@if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	@if [ -d ".cache" ]; then \
		rm -rf .cache; \
	fi
