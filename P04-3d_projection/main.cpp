// Set the engine on
#define OLC_PGE_APPLICATION

// Include the standard headers
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <Eigen/Dense>
#include <iostream>

// Include the main headers
#include "olcPixelGameEngine.h"

using namespace olc;
using namespace std;

using Eigen::Vector3d;
using Eigen::Vector4d;
using Eigen::Matrix4d;
using Eigen::Matrix3d;

static const double PI = 3.14159;

static const int WINDOW_W = 512;
static const int WINDOW_H = 480;

// Functions that give us rotation matrices for each axis

Matrix3d rotateX(double theta)
{
    Matrix3d x_rotation_matrix;
    x_rotation_matrix << 1.0, 0.0, 0.0,
        0.0, cos(theta), -sin(theta),
        0.0, sin(theta), cos(theta);
    return x_rotation_matrix;
}

Matrix3d rotateY(double theta)
{
    Matrix3d y_rotation_matrix;
    y_rotation_matrix << cos(theta), 0.0, sin(theta),
        0.0, 1.0, 0.0,
        -sin(theta), 0.0, cos(theta);
    return y_rotation_matrix;
}

Matrix3d rotateZ(double theta)
{
    Matrix3d z_rotation_matrix;
    z_rotation_matrix << cos(theta), -sin(theta), 0.0,
        sin(theta), cos(theta), 0.0,
        0.0, 0.0, 1.0;
    return z_rotation_matrix;
}

// ========================================================

class Triangle
{
    public:
        Vector3d p1, p2, p3;

        Triangle(Vector3d a, Vector3d b, Vector3d c)
        {
            p1 = a;
            p2 = b;
            p3 = c;
        }

        Vector3d barycenter()
        {
            return (p1 + p2 + p3) / 3.0;
        }

        Vector3d normalVector()
        {
            Vector3d u = p2 - p1;
            Vector3d v = p3 - p1;
            Vector3d normal = u.cross(v);
            return normal.normalized();
        }

        void rotate(Matrix3d rotation_matrix)
        {
            p1 = rotation_matrix * p1;
            p2 = rotation_matrix * p2;
            p3 = rotation_matrix * p3;
        }

        void translate(Vector3d translation)
        {
            p1 += translation;
            p2 += translation;
            p3 += translation;
        }

        void project(Matrix4d projection_matrix)
        {
            Vector4d v1 = projection_matrix * Vector4d(p1(0), p1(1), p1(2), 1.0);
            Vector4d v2 = projection_matrix * Vector4d(p2(0), p2(1), p2(2), 1.0);
            Vector4d v3 = projection_matrix * Vector4d(p3(0), p3(1), p3(2), 1.0);

            if (v1(3) != 0.0) {
                p1 = Vector3d(v1(0) / v1(3), v1(1) / v1(3), v1(2) / v1(3));
            }
            if (v2(3) != 0.0) {
                p2 = Vector3d(v2(0) / v2(3), v2(1) / v2(3), v2(1) / v2(3));
            }
            if (v3(3) != 0.0) {
                p3 = Vector3d(v3(0) / v3(3), v3(1) / v3(3), v3(1) / v3(3));
            }
        }

        void projection_to_screen(int w, int h)
        {
            translate(Vector3d(1.0, -1.0, 0.0));

            double xsc =  0.5 * w;
            double ysc = -0.5 * h;
            Matrix3d to_screen_matrix;
            to_screen_matrix << xsc, 0.0, 0.0,
                                0.0, ysc, 0.0,
                                0.0, 0.0, 1.0;

            p1 = to_screen_matrix * p1;
            p2 = to_screen_matrix * p2;
            p3 = to_screen_matrix * p3;
        }

        void transform_to_view(Matrix4d model_view_matrix)
        {
            // cout << endl << "Matriz Model View: " << endl << model_view_matrix << endl;
            Vector4d v1 = model_view_matrix * Vector4d(p1(0), p1(1), p1(2), 1.0);
            Vector4d v2 = model_view_matrix * Vector4d(p2(0), p2(1), p2(2), 1.0);
            Vector4d v3 = model_view_matrix * Vector4d(p3(0), p3(1), p3(2), 1.0);
            p1 = Vector3d(v1(0), v1(1), v1(2));
            p2 = Vector3d(v2(0), v2(1), v2(2));
            p3 = Vector3d(v3(0), v3(1), v3(2));
        }

};

class Camera
{
    private:
        // Camera's position, direction and up vectors
        Vector3d eye, at, up;

    public:
        void look_at(Vector3d _eye, Vector3d _at, Vector3d _up)
        {
            eye = _eye;
            at = _at;
            up = _up;
        }
        
        // It's essentially a change of basis.
        // This matrix can take a point from the usual RxRxR space
        // and transform it to the "camera's basis".
        Matrix4d model_view()
        {
            Vector3d camz = at.normalized();
            Vector3d camx = up.cross(camz).normalized();
            Vector3d camy = camz.cross(camx).normalized();

            Matrix4d m;
            m << camx(0), camx(1), camx(2), -eye.dot(camx),
                 camy(0), camy(1), camy(2), -eye.dot(camy),
                 camz(0), camz(1), camz(2), -eye.dot(camz),
                 0.0, 0.0, 0.0, 1.0;

            return m;
        }

        void translate(Vector3d translation)
        {
            look_at(eye + translation, at, up);
        }

        void rotate(double theta) // Only working for rotations around Y axis
        {
            Matrix3d rotation_matrix = rotateY(theta);
            look_at(eye, (rotation_matrix * at).normalized(), up);
        }

};

class Projector : public PixelGameEngine
{
    public:
        Projector()
        {
            sAppName = "3D Projection";
        }

    private:
        vector<Triangle> mesh;

        Matrix4d projection_matrix;

        double near = 0.1;
        double far  = 1000.0;
        double fov  = 90.0;
        double aspect_ratio = (double) WINDOW_H / (double) WINDOW_W;

        double thetaX = 0.0;
        double thetaY = 0.0;
        double thetaZ = 0.0;

        Vector3d light_direction = Vector3d(1.0, -1.0, 1.0).normalized();

        Camera camera;

        // Give the projection matrix
        Matrix4d perspective(double fovy, double aspect, double zNear, double zFar) {
            double f = 1.0 / tan((fovy * PI / 180) / 2);
            double d = zFar - zNear;

            Matrix4d result;
            result << f * aspect, 0.0,      0.0, 0.0,
                            0.0,   f,       0.0, 0.0,
                            0.0,  0.0, zFar / d, -(zNear * zFar) / d,
                            0.0,  0.0,       1.0, 0.0;

            return result;
        }

        bool load_mesh_from_file(string path)
        {
            ifstream file_stream(path);
            if (!file_stream.is_open())
            {
                return false;
            }

            vector<Vector3d> vertex_pool;
            while (!file_stream.eof())
            {
                int max_chars = 256;
                char line[max_chars];
                file_stream.getline(line, max_chars);

                stringstream ss;
                ss << line;
                char flag;
                ss >> flag;

                if (flag == 'v')
                {
                    Vector3d v;
                    ss >> v(0) >> v(1) >> v(2);
                    vertex_pool.push_back(v);
                }
                if (flag == 'f')
                {
                    int i0, i1, i2;
                    ss >> i0 >> i1 >> i2;
                    mesh.push_back(Triangle(vertex_pool[i0 - 1], vertex_pool[i1 - 1], vertex_pool[i2 - 1]));
                }
            }
            return true;
        }

        void input(float delta)
        {
            if (IsFocused())
            {
                if (GetKey(Key::UP).bHeld)
                {
                    camera.translate(Vector3d(0,  4 * delta, 0));
                }
                if (GetKey(Key::DOWN).bHeld)
                {
                    camera.translate(Vector3d(0, -4 * delta, 0));
                }
                if (GetKey(Key::RIGHT).bHeld)
                {
                    camera.rotate( 3 * delta);
                }
                if (GetKey(Key::LEFT).bHeld)
                {
                    camera.rotate(-3 * delta);
                }
            }
        }

    bool OnUserCreate() override
    {
        // Defining the projection matrix
        projection_matrix = perspective(fov, aspect_ratio, near, far);

        // Starting values of the camera
        camera.look_at(Vector3d(0,0,0), Vector3d(0,0,1), Vector3d(0,1,0));

        if (!load_mesh_from_file("meta/rocket.obj"))
        {
            return false;
        }

        return true;
    }

    bool OnUserUpdate(float delta) override
    {
        input(delta);

        thetaX = thetaX < 2 * PI ? thetaX + 1.7 * delta : 1.7 * delta;
        thetaY = thetaY < 2 * PI ? thetaY + 1.1 * delta : 1.1 * delta;
        thetaZ = thetaZ < 2 * PI ? thetaZ + 0.9 * delta : 0.9 * delta;

        Matrix3d rotation_matrix = rotateX(thetaX) * rotateY(thetaY) * rotateZ(thetaZ);
        Vector3d cam_position(0, 0, 0);
        vector<Triangle> triangles_to_raster;

        FillRect(0, 0, ScreenWidth(), ScreenHeight(), BLACK);

        for (Triangle t : mesh)
        {
            Triangle t_transformed(t.p1, t.p2, t.p3);
            t_transformed.rotate(rotation_matrix);
            t_transformed.translate(Vector3d(0.0, 0.0, 4.0));
            t_transformed.transform_to_view(camera.model_view());

            Vector3d cam_to_triangle = (t_transformed.barycenter() - cam_position).normalized();
            Vector3d t_normal = t_transformed.normalVector();

            if (cam_to_triangle.dot(t_normal) < 0.0)
            {
                triangles_to_raster.push_back(t_transformed);
            }
        }

        sort(triangles_to_raster.begin(), triangles_to_raster.end(), [](Triangle &t1, Triangle &t2)
        {
            Vector3d barycenter_t1 = t1.barycenter();
            Vector3d barycenter_t2 = t2.barycenter();
            return barycenter_t1(2) > barycenter_t2(2);
        });

        for (Triangle t : triangles_to_raster)
        {
            Vector3d t_normal = t.normalVector();
            t.project(projection_matrix);
            t.projection_to_screen(ScreenWidth(), ScreenHeight());

            int intensity = (int) (128 * (1 - light_direction.dot(t_normal)));
            FillTriangle(t.p1(0), t.p1(1), t.p2(0), t.p2(1), t.p3(0), t.p3(1),
                         Pixel(intensity, intensity, intensity));
        }

        return true;
    }
};

int main()
{
    Projector app;
    if (app.Construct(WINDOW_W, WINDOW_H, 2, 2)) {
        app.Start();
    }
    return 0;
}
