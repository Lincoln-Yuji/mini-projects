// Set the engine on
#define OLC_PGE_APPLICATION

// Include the main headers
#include "olcPixelGameEngine.h"

// Include standard headers
#include <unistd.h>
#include <time.h>
#include <stack>
#include <iostream>
#include <utility>

using namespace olc;
using namespace std;

const int MAZE_W = 30;
const int MAZE_H = 25;

class MazeGenerator : public PixelGameEngine
{
    private:
        bool visited[MAZE_W][MAZE_H];
        int n_visited = 0;

        stack<pair<int, int>> backtrack;
        pair<int, int> directions[4];

    bool OnUserCreate() override
    {
        srand(time(NULL));

        for (int i = 0; i < MAZE_W; i++) {
            for (int j = 0; j < MAZE_H; j++) {
                visited[i][j] = false;
            }
        }

        directions[0] = make_pair( 0, -1);
        directions[1] = make_pair( 0,  1);
        directions[2] = make_pair( 1,  0);
        directions[3] = make_pair(-1,  0);

        FillRect(0, 0, MAZE_W * 16 + 2, MAZE_H * 16 + 2, WHITE);
        for (int x = 0; x < MAZE_W; x++) {
            for (int y = 0; y < MAZE_H; y++) {
                drawSquare(x, y);
            }
        }

        // Let's start from (0, 0)
        backtrack.push(make_pair(0, 0));
        visited[0][0] = true;
        n_visited = 1;

        return true;
    }

    bool OnUserUpdate(float delta) override
    {
        if (n_visited < MAZE_W * MAZE_H) {
            
            vector<pair<int, int>> notVisitedNeighbours;
            int current_x = backtrack.top().first;
            int current_y = backtrack.top().second;

            for (int i = 0; i < 4; i++) {

                int x_neighbour = current_x + directions[i].first;
                int y_neighbour = current_y + directions[i].second;
                
                if (validPosition(x_neighbour, y_neighbour) && !visited[x_neighbour][y_neighbour]) {
                    notVisitedNeighbours.push_back(make_pair(x_neighbour, y_neighbour));
                }

            }

            if (!notVisitedNeighbours.empty()) {
                
                auto neighbour = notVisitedNeighbours[rand() % notVisitedNeighbours.size()];
                visited[neighbour.first][neighbour.second] = true;

                fillPath(backtrack.top(), neighbour);

                backtrack.push(make_pair(neighbour.first, neighbour.second));
                n_visited++;

            }
            else {
                backtrack.pop();            
            }

        }
        usleep(6000);
        return true;
    }

    private:
        bool validPosition(int x, int y) {
            return ((x >= 0 && x < MAZE_W) && (y >= 0 && y < MAZE_H));
        }

        void drawSquare(int x, int y) {
            int px = x * 16 + 1;
            int py = y * 16 + 1;
            for (int xOffset = 1; xOffset < 15; xOffset++) {
                for (int yOffset = 1; yOffset < 15; yOffset++) {
                    Draw(px + xOffset, py + yOffset, BLUE);
                }
            }
        }

        void fillPath(pair<int,int> p1, pair<int,int> p2) {
            int px, py, width, height;
            if (p1.first == p2.first) {
                width  = 15;
                height = 31;
                if (p1.second < p2.second) {
                    px = p1.first * 16 + 1;
                    py = p1.second * 16 + 1;
                }
                else {
                    px = p2.first * 16 + 1;
                    py = p2.second * 16 + 1;
                }
            }
            else {
                width  = 31;
                height = 15;
                if (p1.first < p2.first) {
                    px = p1.first * 16 + 1;
                    py = p1.second * 16 + 1;
                }
                else {
                    px = p2.first * 16 + 1;
                    py = p2.second * 16 + 1;
                }
            }
            FillRect(px, py, width, height, BLACK);
        }
};

int main()
{
    MazeGenerator app;
    if (app.Construct(MAZE_W * 16 + 2, MAZE_H * 16 + 2, 2, 2)) {
        app.Start();
    }
    return 0;
}
