#include <unistd.h>

void mergeSort(int arr[], int n);

void mergeSortAux(int arr[], int tmp[], int start, int end) {
    if (start < end) {

        int mid = (start + end) / 2;
        mergeSortAux(arr, tmp, start, mid);
        mergeSortAux(arr, tmp, mid+1, end);

        int i = start;
        int j = mid + 1;
        int k = start;
        while (i <= mid && j <= end) {
            if (arr[i] < arr[j]) {
                tmp[k++] = arr[i++];
            }
            else {
                tmp[k++] = arr[j++];
            }
        }
        while (i <= mid) {
            tmp[k++] = arr[i++];
        }
        while (j <= end) {
            tmp[k++] = arr[j++];
        }
        for (k = start; k <= end; k++) {
            arr[k] = tmp[k];
            usleep(10000);
        }
    }
}

void mergeSort(int arr[], int n) {
    int tmp[n];
    mergeSortAux(arr, tmp, 0, n-1);
}