// Set the engine on
#define OLC_PGE_APPLICATION

// Include the main headers
#include "olcPixelGameEngine.h"
#include "sort.h"

// Include the standard headers
#include <thread>
#include <time.h>

using namespace std;
using namespace olc;

#define MAX 400 // Give preference to dividers of SCREEN_W

const int SCREEN_W = 800;
const int SCREEN_H = 600;

void swap(int* a, int* b) {
    int aux = *a;
    *a = *b;
    *b = aux; 
}

class Application : public PixelGameEngine
{   
    public:
        int arr[MAX];

    bool OnUserCreate() override
    {
        FillRect(0, 0, SCREEN_W, SCREEN_H, BLACK);
        return true;
    }

    bool OnUserUpdate(float delta) override
    {
        render();
        return true;
    }

    void render() {
        FillRect(0, 0, SCREEN_W, SCREEN_H, BLACK);

        int w = SCREEN_W / MAX;
        for (int i = 0; i < MAX; i++) {
            int h = (arr[i] * SCREEN_H) / MAX;
            FillRect(i * w, SCREEN_H - h, w, h, WHITE);
        }
    }
};

int main() {
    Application app;
    if (app.Construct(SCREEN_W, SCREEN_H, 1, 1)) {

        srand(time(NULL));

        // Fill the array
        for (int i = 0; i < MAX; i++) {
            app.arr[i] = i;
        }
        // Shuffle the array
        for (int i = 0, j = rand() % MAX; i < MAX; i++, j = rand() % MAX) {
            swap(&app.arr[i], &app.arr[j]);
        }
        // Sort it in another thread
        thread sorting(mergeSort, app.arr, MAX);

        app.Start();
    }
    return 0;
}
